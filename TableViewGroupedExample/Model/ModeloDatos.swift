//
//  ModeloDatos.swift
//  TableViewGroupedExample
//
//  Created by mbtec22 on 4/29/21.
//  Copyright © 2021 Tecsup. All rights reserved.
//

import UIKit

class ModeloDatos {

    func obtenerSeccionesDesdeDatos() -> [Secciones]{
        
        //Creamos array de secciones
        var seccionesArray = [Secciones]()
        
        let peliculas = Secciones(titulo: "titulo de pelicula", objetos:["pelicula 1","pelicula 2", "pelicula 3", "pelicula 4"])
        
        let actores = Secciones(titulo: "Actores", objetos: ["actor 1", "actor 2", "actor 3", "actor 4"])
        
        let ciudades = Secciones(titulo: "Ciudades del mundo", objetos: ["Madrid", "Lima", "Buenos Aires"])
        
        //Cargamos nuestro array de secciones
        seccionesArray.append(peliculas)
        seccionesArray.append(actores)
        seccionesArray.append(ciudades)
        return seccionesArray
        
    }
    
}
