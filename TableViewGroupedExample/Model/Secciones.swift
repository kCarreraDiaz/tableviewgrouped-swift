
// Creamos estructra para el diseño de la tabla (MODELO DE DATOS)
struct Secciones {
    
    //variables de ml modelo
    var cabecera: String
    var items: [String]
    
    //inicializamos las variables
    init(titulo: String, objetos: [String]) {
        cabecera = titulo
        items = objetos
    }
}
